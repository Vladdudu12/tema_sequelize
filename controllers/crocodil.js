const CrocodilDb = require('../models').Crocodil;
const InsulaDb = require('../models').Insula;

const controller = {
    getAllCrocodiles: async (req, res) => {
        CrocodilDb.findAll().then((crocodili) => {
            res.status(200).send(crocodili);
        }).catch((err) => {
            console.log(err)
            res.status(500).send({ message: "Server Error" });
        });
    },

    addCrocodile: async (req, res) => {
        const { idInsula, nume, prenume, varsta } = req.body;
        InsulaDb.findByPk(idInsula).then((insula) => {
            if (insula) {
                insula.createCrocodil({ nume, prenume, varsta }).then((crocodil) => {
                    res.status(201).send(crocodil);
                }).catch((error) => {
                    console.log(error);
                    res.status(500).send({ message: "Server error" });
                });
            }
            else {
                res.status(404).send({ message: "Insula not found" });
            }
        }).catch((error) => {
            console.log(error);
            res.status(500).send({ message: "Server error" });
        });
    },

    //#region TEMA
    deleteCrocodile: async (req, res) => {
        const { id } = req.params;
        CrocodilDb.findByPk(id).then((crocodil) =>{
            if(crocodil){
                crocodil.destroy().then(()=>
                {
                    res.status(200).send({message : "Crocodil Sters"});
                }).catch((error) =>{
                    console.log(error);
                    res.status(500).send({ message: "Server error" });
                });
            }
            else{
                res.status(404).send({ message: "Crocodile not found"});
            }
        }).catch((error) => {
            console.log(error);
            res.status(500).send({ message: "Server error" });
        });
    },

    updateCrocodile: async (req, res) =>{
        const { id } = req.params;
        const { nume } = req.body;
        CrocodilDb.findByPk(id).then((crocodil) => {
            if(crocodil) {
                crocodil.update({ nume: nume }).then(() =>{
                    res.status(200).send({message: "Crocodil modificat"});
                }).catch((error) =>{
                    console.log(error);
                    res.status(500).send({ message: "Server error" });
                });
            }
            else{
                res.status(404).send({message: "Crocodile not found"});
            }
        }).catch((error) =>{
            console.log(error);
            res.status(500).send({ message: "Server error" });
        });
    },
    //#endregion
};

module.exports = controller;