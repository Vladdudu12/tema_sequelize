const express = require('express');
const router = express.Router();
const crocodilController = require('../controllers').crocodil;

router.get('/', crocodilController.getAllCrocodiles);
router.post('/', crocodilController.addCrocodile);
router.delete('/:id', crocodilController.deleteCrocodile);
router.put('/:id', crocodilController.updateCrocodile);
module.exports = router;